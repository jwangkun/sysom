import react, {useState,useEffect} from 'react';
import ProForm, {ProFormSelect, ProFormRadio, ProFormText, ProFormCheckbox} from '@ant-design/pro-form';
import {Button,message} from 'antd';
import { useRequest, useIntl, FormattedMessage } from 'umi';
import ProCard from '@ant-design/pro-card';
import {queryAssessHost,queryStartAssess} from '../../../service'
// querySqlFile
import './StAssessmentForm.less';

export default (props) => {
  const intl = useIntl();
  const [hostList,setHostList] = useState([]);
  const [sqlFileList,setSqlFileList] = useState([]);
  // Repo配置选择项
  const [repoType, setRepoType] = useState(false);
  // 评估应用是否展示
  const [appType, setAppType] = useState(false);
  
  const initialValues = {
    repo_type: 'public',
    version: 'Anolis OS 8',
    ass_type: ['mig_imp']
  };

  useEffect(()=>{
    getAssessHost();
    // getSqlFile();
  },[]);

  const getAssessHost = async () => {
    const {data} = await queryAssessHost();
    let arr = [];
    if(data?.length > 0){
      data.forEach((i)=>{
        arr.push({label: i.ip,value: i.ip})
      })
    }
    setHostList(arr);
  }

  // const getSqlFile = async () => {
  //   const {data} = await querySqlFile();
  //   setSqlFileList(data?data:[]);
  // }

  const handleRepo = (e) => {
    if(e.target.value === "public"){
      setRepoType(false);
    }else if(e.target.value === "private"){
      setRepoType(true);
    }
  }

  const handleType = (e) => {
    let isShow = false;
    e?.length > 0 && e.forEach((i)=>{
      if(i === 'mig_app'){
        isShow = true;
      }
    });
    setAppType(isShow);
  }

  // 开始评估的接口
  const { loading, error, run } = useRequest(queryStartAssess, {
    manual: true,
    onSuccess: (result, params) => {
      message.success('开始评估成功')
      // 开始评估成功后刷新列表
      props?.success();
    },
    onError:(data)=>{
      console.log('请求错误',error,)
    },
  });

    return (
      <ProCard>
        <ProForm
          onFinish={async (values) => {
            run(values);
          }}
          submitter={{
            submitButtonProps: {
              style: {
                display: "none",
              },
            },
            resetButtonProps: {
              style: {
                display: "none",
              },
            },
          }}
          layout={"horizontal"}
          autoFocusFirstInput
          initialValues={initialValues}
        >
          <ProForm.Group>
            <ProFormSelect
              name="ip"
              label={intl.formatMessage({
                id: 'pages.migrate.selectivemachine',
                defaultMessage: 'Selective machine',
              })}
              width="sm"
              options={hostList}
              fieldProps={{
                mode: "multiple",
              }}
              placeholder={intl.formatMessage({
                id: 'pages.migrate.selectmachine_please',
                defaultMessage: 'Please select machine',
              })}
              rules={[
                { required: true, message: <FormattedMessage id="pages.migrate.selectmachine_placeholder" defaultMessage="The machine cannot be empty" />, type: "array" },
              ]}
            />
            <ProFormSelect
              name="version"
              label={intl.formatMessage({
                id: 'pages.migrate.migratingversion',
                defaultMessage: 'Migrating version',
              })}
              width="sm"
              options={[
                {
                  label: "Anolis OS 8",
                  value: "Anolis OS 8",
                },
              ]}
              placeholder={intl.formatMessage({
                id: 'pages.migrate.migratingversion_please',
                defaultMessage: 'Select the migration version',
              })}
              rules={[{ required: true, message: <FormattedMessage id="pages.migrate.migratingversion_placeholder" defaultMessage="The migration version cannot be empty" /> }]}
            />
            {/* <ProFormSelect
              name="sqlfile"
              label="数据文件"
              width="sm"
              options={sqlFileList}
              placeholder="请选择数据文件"
              rules={[{ required: true, message: "数据文件不能为空" }]}
            /> */}
            <ProFormRadio.Group
              name="repo_type"
              label={intl.formatMessage({
                id: 'pages.migrate.publicaddress',
                defaultMessage: 'Repo configuration',
              })}
              options={[
                {
                  label: <FormattedMessage id="pages.migrate.publicaddress" defaultMessage="Public address" />,
                  value: "public",
                },
                {
                  label: <FormattedMessage id="pages.migrate.intranetaddress" defaultMessage="Intranet address" />,
                  value: "private",
                },
              ]}
              onChange={handleRepo}
            />
            {repoType && (
              <ProFormText
                colProps={{ span: 24 }}
                name="repo_url"
                width='sm'
                label=''
                placeholder={intl.formatMessage({
                  id: 'pages.migrate.intranetaddress_please',
                  defaultMessage: 'Please enter the Intranet address',
                })}
              />
            )}
          </ProForm.Group>
          <ProForm.Group>
            <ProFormCheckbox.Group
              name="ass_type"
              label={intl.formatMessage({
                id: 'pages.migrate.selectiveevaluation',
                defaultMessage: 'Selective evaluation',
              })}
              rules={[{ required: true, message: <FormattedMessage id="pages.migrate.selectiveevaluation_placeholder" defaultMessage="The estimate cannot be empty" /> }]}
              onChange={handleType}
              options={[
                { label: <FormattedMessage id="pages.migrate.riskassessment" defaultMessage="Risk assessment" />, value: 'mig_imp', disabled: true },
                { label: <FormattedMessage id="pages.migrate.systemevaluation" defaultMessage="System evaluation" />, value: 'mig_sys' },
                { label: <FormattedMessage id="pages.migrate.hardwareevaluation" defaultMessage="Hardware evaluation" />, value: 'mig_hard' },
                { label: <FormattedMessage id="pages.migrate.applicationevaluation" defaultMessage="Application evaluation" />, value: 'mig_app' },
              ]}
            />
            {appType && (
              <ProFormText
                name="ass_app"
                width='sm'
                label=""
                placeholder={intl.formatMessage({
                  id: 'pages.migrate.applicationevaluation_please',
                  defaultMessage: 'Please enter evaluation application',
                })}
              />
            )}
            <Button
              className="st_form_start"
              type="primary"
              htmlType="submit"
              // loading={loading}
            >
              <FormattedMessage id="pages.migrate.startassessment" defaultMessage="Start assessment" />
            </Button>
          </ProForm.Group>
        </ProForm>
      </ProCard>
    );
}
